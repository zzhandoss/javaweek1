package models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
    private Connection connection;

    public Connection getConnection() throws SQLException {
        Connection result;
        final String username = "postgres";
        final String password = "admin";
        final String url = "jdbc:postgresql://127.0.0.1:5432/task1";
        if(connection != null){
            result = connection;
        } else {
            result = DriverManager.getConnection(url,username,password);
        }
        return result;


    }
//    private void closeConnection() throws SQLException{
//        if(this.connection != null){
//            this.connection = null;
//        }
//    }
}
